# -*- coding: utf-8 -*-
import sys

from PySide6 import QtGui, QtCore, QtWidgets
from PySide6.QtCore import QMetaObject, QRect, Qt
from PySide6.QtGui import QFont, QMouseEvent
from PySide6.QtWidgets import QApplication, QComboBox, QMainWindow, QWidget, QPushButton, QLabel, QLineEdit, \
    QTableWidget, QAbstractItemView, QFrame
from lists import level_dict, search_edit, tips_dict, level_dict_plus, battle_info_dict, dict_m_n
import 英雄头像
import 阵容等级图
import 装备图
import 顶部图标


class Ui_MainWindow(object):
    battles_dict = level_dict

    def setupUi(self, MainWindow):
        MainWindow.setObjectName(u"MainWindow")
        MainWindow.setWindowFlags(Qt.WindowType.WindowStaysOnTopHint)
        MainWindow.resize(660, 750)
        MainWindow.setWindowTitle(u"云顶阵容查看器")
        icon = QtGui.QPixmap(':/英雄头像/icon.png')
        MainWindow.setWindowIcon(icon)

        MainWindow.setWindowOpacity(0.95)
        flag = Qt.WindowType(Qt.WindowType.Window)
        flag |= Qt.WindowType.FramelessWindowHint
        flag |= Qt.WindowType.MSWindowsFixedSizeDialogHint
        flag |= Qt.WindowType.WindowStaysOnTopHint
        MainWindow.setWindowFlags(flag)
        MainWindow.setStyleSheet(u"background-color: rgb(43, 43, 43)")
        MainWindow.setFocusPolicy(Qt.FocusPolicy.ClickFocus)
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.centralwidget.setFocusPolicy(Qt.FocusPolicy.ClickFocus)
        self.title_image_bar = DoubleClickLabel(self.centralwidget)
        self.title_image_bar.setGeometry(0, 0, 660, 30)
        self.title_image_bar.setFrameStyle(QFrame.Shape.Box | QFrame.Shadow.Raised)
        self.title_image_bar.setStyleSheet("background-color: rgb(75, 75, 75)")

        self.title_icon = QLabel(self.title_image_bar)
        self.title_icon.setGeometry(10, 4, 22, 22)
        self.title_icon.setObjectName('标题顶部图')
        self.title_icon.setStyleSheet(u"border-image: url(:/顶部图标/icon.png);")

        self.minbtn = QPushButton(self.title_image_bar)
        self.minbtn.setGeometry(625, 2, 25, 25)
        self.minbtn.setStyleSheet(u"border-image: url(:/顶部图标/top1.png);")
        self.minbtn.clicked.connect(self.win_max_min)
        self.minbtn.setEnabled(True)

        self.maxbtn1 = QPushButton(self.title_image_bar)
        self.maxbtn1.setGeometry(590, 2, 25, 25)
        self.maxbtn1.setStyleSheet(u"border-image: url(:/顶部图标/top2.png);")
        self.maxbtn1.clicked.connect(self.win_max_min)
        self.maxbtn1.setEnabled(True)

        self.tip = QLabel(self.title_image_bar)
        self.tip.setGeometry(240, 2, 290, 30)
        font = QFont()
        font.setPointSize(9)
        self.tip.setFont(font)
        self.tip.setText('鼠标悬停于此,查看使用说明!!')
        self.tip.setStyleSheet('color:rgb(0,255,0)')
        self.tip.setToolTip(
            '<html><head/><body><p><span style=" font-size:12pt; color:#0000ff;">      '
            '点击右侧图标、双击此状态栏、双击此状态栏下方图片都可以切换窗口大小,</span></p><p><span '
            'style=" font-size:12pt; color:#0000ff;">窗口可拖动停靠位置,如需退出请右击桌面底部状态栏选择退出.'
            '</span></p></body></html>')

        self.title_text = QLabel(self.title_image_bar)
        self.title_text.setGeometry(45, 4, 105, 22)
        self.title_text.setText('云顶阵容查看器')
        self.title_text.setObjectName('title')
        font = QFont()
        font.setPointSize(10)
        self.title_text.setFont(font)
        self.title_text.setStyleSheet(u"color: rgb(255, 255, 255)")

        self.title_image_laber = DoubleClickLabel(self.centralwidget)
        self.title_image_laber.setGeometry(0, 30, 660, 25)
        self.title_image_laber.setObjectName('标题顶部图')
        self.title_image_laber.setStyleSheet(u"border-image: url(:/顶部图标/title.png);")

        self.appinfo_label = QLabel(self.centralwidget)
        self.appinfo_label.setGeometry(25, 60, 620, 30)
        self.appinfo_label.setWordWrap(True)
        self.appinfo_label.setStyleSheet(u"color: rgb(255, 0, 0)")
        self.appinfo_label.setText(
            '    本软件基于Python3.9 | PySide6.4.2 环境开发 支持MacOS Linux Win7(不包括)以上系统 开发许可证:GNU 库或宽通用公'
            '共许可证 （LGPL）、其他/专有许可证 （LGPL）')

        # 特质搜索下拉框
        self.comboBox = QComboBox(self.centralwidget)
        self.comboBox.setObjectName(u"comboBox")
        self.comboBox.setGeometry(QRect(48, 98, 120, 28))
        self.comboBox.setPlaceholderText('特质')
        self.comboBox.setFocusPolicy(Qt.FocusPolicy.ClickFocus)

        list_s1 = ['全部', 'AI程序', '幻灵战队', '平民英雄', '堕落使者', '小天才', '源计划:激光特工', '战斗机甲',
                   '福牛守护者', '星之守护者', '超级英雄', '怪兽', '地下魔盗团']
        self.comboBox.addItems(list_s1)
        self.comboBox.setMaxVisibleItems(13)
        self.comboBox.setStyleSheet(u"color: rgb(255, 255, 255);")
        self.comboBox.currentTextChanged.connect(self.showinfo)
        # 职业搜索下拉框
        self.comboBox1 = QComboBox(self.centralwidget)
        self.comboBox1.setObjectName(u"comboBox1")
        self.comboBox1.setGeometry(QRect(270, 98, 120, 28))
        self.comboBox1.setPlaceholderText('职业')
        list_s2 = ['全部', '精英战士', '秘术卫士', '枪神', '斗士', '护卫', '决斗大师', '气象主播', '黑客', '爱心使者',
                   '吉祥物', '淘气包', '情报特工', '混沌战士', '灵能使', '枪手']
        self.comboBox1.addItems(list_s2)
        self.comboBox1.setMaxVisibleItems(16)
        self.comboBox1.setStyleSheet(u"color: rgb(255, 255, 255);")
        self.comboBox1.currentTextChanged.connect(self.showinfo1)
        self.comboBox1.setFocusPolicy(Qt.FocusPolicy.ClickFocus)

        # 模糊搜索输入框
        self.search_edit = QLineEdit(self.centralwidget)
        self.search_edit.setObjectName('search_edit')
        self.search_edit.setGeometry(QRect(490, 98, 100, 28))
        self.search_edit.setPlaceholderText('智能模糊搜索阵容')
        self.search_edit.setStyleSheet(u"color: rgb(160, 160, 100);")
        self.search_edit.setToolTip('智能模糊搜索,例:搜索薇恩可输入Uzi或VN等,搜索鳄鱼可输入神超')
        self.search_edit.setFocusPolicy(Qt.FocusPolicy.ClickFocus)
        self.search_edit.returnPressed.connect(self.showinfo2)
        self.search_btn = QPushButton(self.centralwidget)
        self.search_btn.setObjectName('search_btn')
        self.search_btn.setGeometry(QRect(590, 98, 40, 28))
        self.search_btn.setText('搜索')
        self.search_btn.setStyleSheet(u"color: rgb(160, 160, 100);")
        self.search_btn.setToolTip('智能模糊搜索,例:搜索薇恩可输入Uzi或VN等,搜索鳄鱼可输入神超')
        self.search_btn.setFocusPolicy(Qt.FocusPolicy.ClickFocus)
        self.search_btn.clicked.connect(self.showinfo2)
        #  阵容显示表格
        self.listbox = QTableWidget(self.centralwidget)
        self.listbox.setObjectName('结果列表')
        self.listbox.verticalHeader().setVisible(False)
        self.listbox.horizontalHeader().setVisible(False)
        self.listbox.setEditTriggers(QAbstractItemView.EditTrigger.NoEditTriggers)
        self.listbox.setSelectionBehavior(QAbstractItemView.SelectionBehavior.SelectItems)
        self.listbox.resizeColumnsToContents()
        self.listbox.horizontalHeader().setStretchLastSection(True)
        self.listbox.setGeometry(QRect(25, 135, 610, 580))
        self.listbox.setFocusPolicy(Qt.FocusPolicy.ClickFocus)
        self.show_all()

        self.exit_label = QLabel(self.centralwidget)
        self.exit_label.setGeometry(0, 720, 660, 20)
        self.exit_label.setAlignment(Qt.AlignmentFlag.AlignCenter)
        self.exit_label.setText('如需退出使用,请右击桌面窗口下底部状态栏选择退出!!!')
        self.exit_label.setStyleSheet('color: rgb(0,255,0)')

        MainWindow.setCentralWidget(self.centralwidget)
        QMetaObject.connectSlotsByName(MainWindow)

    def showinfo1(self):
        text = self.comboBox1.currentText()
        if self.comboBox1.currentIndex() == 0:
            self.comboBox.setCurrentIndex(0)
            self.search_edit.clear()
            self.battles_dict = level_dict
            self.show_all()
        else:
            self.comboBox.setCurrentIndex(0)
            self.search_edit.clear()
            self.battles_dict = search_edit(text)
            self.show_all()

    def showinfo2(self):
        text = self.search_edit.text()
        if text:
            self.comboBox.setCurrentIndex(0)
            self.comboBox1.setCurrentIndex(0)
            self.battles_dict = search_edit(text)
            self.show_all()

    def showinfo(self):
        text = self.comboBox.currentText()
        if self.comboBox.currentIndex() == 0:
            self.comboBox1.setCurrentIndex(0)
            # self.search_edit.clear()
            self.battles_dict = level_dict
            self.show_all()
        else:
            self.comboBox1.setCurrentIndex(0)
            # self.search_edit.clear()
            self.battles_dict = search_edit(text)
            self.show_all()

    def show_nobattle(self):
        self.listbox.setRowCount(0)
        self.listbox.setColumnCount(0)
        self.listbox.setRowCount(1)
        self.listbox.setColumnCount(1)
        self.listbox.setRowHeight(0, 200)
        self.listbox.setColumnWidth(0, 600)
        data = QLabel()
        data.setGeometry(0, 150, 600, 50)
        font = QFont()
        font.setBold(True)
        font.setPointSize(14)
        data.setFont(font)
        data.setObjectName('no_battle')
        data.setStyleSheet(u"color: rgb(230, 190, 100);")
        data.setText('没有找到你想要搜索的阵容,请重新查找!!!')
        data.setAlignment(Qt.AlignmentFlag.AlignCenter)
        # data.setFrameStyle(QFrame.Shape.Box | QFrame.Shadow.Raised)
        self.listbox.setCellWidget(0, 0, data)

    def show_all(self):
        if not self.battles_dict:
            self.show_nobattle()
            return
        self.listbox.setRowCount(0)
        row = len(self.battles_dict)
        self.listbox.setRowCount(row)
        self.listbox.setColumnCount(4)
        column1_list = []
        self.column2_list = []
        self.column3_list = []
        self.column2_list0 = []
        for i in self.battles_dict:
            column1_list.append(self.battles_dict.get(i)[0])
            self.column2_list.append(self.battles_dict.get(i)[2:])
            self.column2_list0.append(self.battles_dict.get(i)[1])
            self.column3_list.append(level_dict_plus.get(i))  # [[[],[]],[[],[]]]
        for j in range(row):
            self.listbox.setRowHeight(j, 55)  # 设置表格单行高度为60,列宽为:60+120+550+50 = 730 +20 (右侧滚动条)
            self.listbox.setColumnWidth(0, 50)
            self.listbox.setColumnWidth(1, 90)
            self.listbox.setColumnWidth(2, 400)
            self.listbox.setColumnWidth(3, 40)
            column1 = column1_list[j]  # 图标名称字符串
            column2 = self.column2_list[j]  # 三个符文组成的列表
            column2_row0 = self.column2_list0[j]  # 阵容名称组成的列表
            column3 = self.column3_list[j]  # 阵容英雄组成的列表[[英雄,装备...],[英雄]]
            column1_label = QPushButton()  # 每行的第一列 插入阵容等级图标
            column1_label.setEnabled(False)
            column1_label_btn = QLabel(column1_label)
            column1_label_btn.setGeometry(0, 0, 50, 52)
            column1_label_btn.setObjectName('阵容等级')
            column1_label_btn.setStyleSheet(u"border-image: url(:/阵容等级图/%s);" % column1)
            self.listbox.setCellWidget(j, 0, column1_label)
            column2_label = QPushButton()  # 每行的第二列  第一行插入阵容名,第二行插入三个符文的图标
            column2_label.setEnabled(False)
            column2_label0 = QLabel(column2_label)  # 第一行的内容
            column2_label0.setGeometry(0, 0, 88, 30)
            column2_label0.setFrameStyle(QFrame.Shape.Box | QFrame.Shadow.Raised)
            column2_label0.setLineWidth(2)
            column2_label0.setText(column2_row0)
            column2_label0.setAlignment(Qt.AlignmentFlag.AlignCenter)
            font = QFont()
            font.setBold(False)
            font.setPointSize(9)
            column2_label0.setFont(font)
            column2_label0.setStyleSheet(u"color: rgb(230, 190, 100);")
            column2_label0.setToolTip(column2_row0)
            column2_label0.setGeometry(0, 0, 90, 27)
            column2_label1 = QLabel(column2_label)  # 第二行的内容
            column2_label1.setGeometry(0, 27, 28, 28)
            column2_label1.setObjectName(column2[0][:-4])
            column2_label1.setToolTip(str(column2[0][:-4]) + ':' + str(tips_dict.get(column2[0][:-4])))
            column2_label1.setStyleSheet("#%s{border-image:url(:/阵容等级图/%s)}" % (column2[0][:-4], column2[0]))
            column2_label2 = QLabel(column2_label)
            column2_label2.setGeometry(30, 27, 28, 28)
            column2_label2.setObjectName(column2[1][:-4])
            column2_label2.setToolTip(str(column2[1][:-4]) + ':' + str(tips_dict.get(column2[1][:-4])))
            column2_label2.setStyleSheet("#%s{border-image:url(:/阵容等级图/%s)}" % (column2[1][:-4], str(column2[1])))
            column2_label3 = QLabel(column2_label)
            column2_label3.setGeometry(60, 27, 28, 28)
            column2_label3.setObjectName(column2[2][:-4])
            column2_label3.setToolTip(str(column2[2][:-4]) + ':' + str(tips_dict.get(column2[2][:-4])))
            column2_label3.setStyleSheet("#%s{border-image:url(:/阵容等级图/%s)}" % (column2[2][:-4], column2[2]))
            self.listbox.setCellWidget(j, 1, column2_label)
            column3_label = QPushButton()  # 每行的第三列插入阵容的英雄头像图片
            column3_label.setEnabled(False)
            for i in range(len(column3)):
                column3_label_pic = QLabel(column3_label)
                column3_label_pic.setGeometry(44 * i, 0, 39, 39)
                column3_label_pic.setObjectName(column3[i][0])
                column3_label_pic.setStyleSheet(
                    "#%s{border-image: url(:/英雄头像/%s.png)}" % (column3[i][0], column3[i][0]))
                column3_label_pic.setToolTip(column3[i][0])
                if len(column3[i]) > 1:
                    if len(column3[i]) == 2:
                        h = 15
                    elif len(column3[i]) == 3:
                        h = 7
                    else:
                        h = 0
                    zb_label = QLabel(column3_label)
                    zb_label.setAlignment(Qt.AlignmentFlag.AlignHCenter)
                    zb_label.setGeometry(44 * i, 39, 42, 15)
                    for index in range(1, len(column3[i])):
                        zb_label_sun = QLabel(zb_label)
                        zb_label_sun.setGeometry(h + (index - 1) * 15, 0, 15, 15)
                        zb_label_sun.setObjectName(column3[i][index])
                        zb_label_sun.setStyleSheet(
                            "#%s{border-image: url(:/装备图/%s.png)}" % (column3[i][index], column3[i][index]))
                        zb_label_sun.setToolTip(column3[i][index])
            self.listbox.setCellWidget(j, 2, column3_label)
            column4_btn = QPushButton()  # 每行的第四列插入阵容详情按钮及点击事件
            column4_btn.setGeometry(0, -2, 100, 100)
            column4_btn.setObjectName(str(column3))
            font = QFont()
            font.setBold(True)
            font.setPointSize(12)
            column4_btn.setStyleSheet(u"color: rgb(160, 160, 100);")
            column4_btn.setText('阵容详情')
            column4_btn.setFocusPolicy(Qt.FocusPolicy.ClickFocus)
            column4_btn.setToolTip('点击查看阵容详情')
            column4_btn.setEnabled(True)
            column4_btn.clicked.connect(self.battel_info)
            self.listbox.setCellWidget(j, 3, column4_btn)

    def battel_info(self):  # 阵容详情页面
        index = self.listbox.currentRow()
        battle = self.column3_list[index]
        title = self.column2_list0[index]
        fw_t_list = self.column2_list[index]
        battle_info_dict_key = title[1:-1]
        objectname = battle_info_dict[battle_info_dict_key][0]
        eq_list = battle_info_dict[battle_info_dict_key][1]
        hero_eq_t_list = []
        hero_eq_list = battle_info_dict[battle_info_dict_key][2]
        hero_eq_t_list.append(hero_eq_list)
        for i in range(1, len(battle)):
            if len(battle[i]) > 1:
                lis = []
                for j in battle[i]:
                    j += '.png'
                    lis.append(j)
                hero_eq_t_list.append(lis)
        self.listbox.setRowCount(0)
        self.listbox.setRowCount(5)
        self.listbox.setColumnCount(1)
        self.listbox.setRowHeight(0, 135)  # 设置表格第一行高度
        title_btn = QLabel()  # 新建按钮标签用用插入表格第一行第一列
        title_btn.setGeometry(0, 0, 570, 30)
        title_label = QLabel(title_btn)  # 新建标签用于插入表格第一行的按钮中的左侧
        title_label.setText(title)
        title_label.setObjectName(title)
        title_label.setFrameStyle(QFrame.Shape.Box | QFrame.Shadow.Raised)
        title_label.setLineWidth(2)
        title_label.setGeometry(0, 0, 18 * len(title), 30)
        title_label.setAlignment(Qt.AlignmentFlag.AlignLeft)
        font = QFont()
        font.setBold(True)
        font.setPointSize(12)
        title_label.setFont(font)
        title_label.setStyleSheet("#%s{color: rgb(160, 160, 100)}" % title)
        battle_c1_mid = QLabel(title_btn)  # 第一行的中间插入包函阵容的英雄头像图片的标签  battle_c1_mid
        # battle_c1_mid.setEnabled(False)
        battle_c1_mid.setGeometry(60, 31, len(battle) * 47, 63)
        for i in range(len(battle)):
            battle_c1_mid_pic = QLabel(battle_c1_mid)
            battle_c1_mid_pic.setGeometry(47 * i, 5, 40, 40)
            battle_c1_mid_pic.setObjectName(battle[i][0])
            battle_c1_mid_pic.setStyleSheet(
                "#%s{border-image: url(:/英雄头像/%s.png)}" % (battle[i][0], battle[i][0]))
            battle_c1_mid_pic.setToolTip(battle[i][0])
            if len(battle[i]) > 1:
                if len(battle[i]) == 2:
                    h = 15
                elif len(battle[i]) == 3:
                    h = 7
                else:
                    h = 0
                zb_label = QLabel(battle_c1_mid)
                zb_label.setAlignment(Qt.AlignmentFlag.AlignHCenter)
                zb_label.setGeometry(47 * i, 40, 40, 15)
                for index in range(1, len(battle[i])):
                    zb_label_sun = QLabel(zb_label)
                    zb_label_sun.setGeometry(h + (index - 1) * 13, 0, 13, 13)
                    zb_label_sun.setObjectName(battle[i][index])
                    zb_label_sun.setStyleSheet(
                        "#%s{border-image: url(:/装备图/%s.png)}" % (battle[i][index], battle[i][index]))
                    zb_label_sun.setToolTip(battle[i][index])
        title_label1 = QLabel(title_btn)
        title_label1.setAlignment(Qt.AlignmentFlag.AlignLeft)
        title_label1.setText(str(objectname))  # 表格第一行中的第二分行插入阵容羁绊详情标签
        title_label1.setObjectName(objectname)
        title_label1.setGeometry(10, 88, 580, 40)  # 19*len(title)+5
        font = QFont()
        font.setPointSize(11)
        title_label1.setFont(font)
        title_label1.setStyleSheet(u"color: rgb(37, 171, 250);")
        title_label1.setWordWrap(True)
        title_back_btn = QPushButton(title_btn)  # 第一行最右侧返回按钮
        title_back_btn.setGeometry(520, 5, 65, 35)
        title_back_btn.setText('返回列表')
        title_back_btn.setFocusPolicy(Qt.FocusPolicy.ClickFocus)
        title_back_btn.setObjectName('返回列表')
        font = QFont()
        font.setBold(True)
        font.setPointSize(11)
        title_back_btn.setFont(font)
        title_back_btn.setStyleSheet("#返回列表{color: rgb(255, 255, 0)}")
        title_back_btn.clicked.connect(self.show_all)
        title_btn.setFrameStyle(QFrame.Shape.Box | QFrame.Shadow.Raised)
        title_btn.setLineWidth(2)
        self.listbox.setCellWidget(0, 0, title_btn)

        #  以上完成对阵容详情页表格数据第一行按钮的内容设置
        #  以下是对阵容详情页表格第二行数据设置
        self.listbox.setRowHeight(1, 53)  # 设置表格的第二行的高度
        fw_label = QLabel()
        fw_label.setFrameStyle(QFrame.Shape.Box | QFrame.Shadow.Raised)
        fw_label.setLineWidth(2)
        fw_label_left = QLabel(fw_label)
        font = QFont()
        font.setBold(True)
        font.setPointSize(12)
        fw_label_left.setFont(font)
        fw_label_left.setGeometry(0, 0, 125, 30)
        fw_label_left.setFrameStyle(QFrame.Shape.Box | QFrame.Shadow.Raised)
        fw_label_left.setLineWidth(2)
        fw_label_left.setObjectName('阵容符文')
        fw_label_left.setText('【阵容符文】')
        fw_label_left.setStyleSheet("#阵容符文{color: rgb(160, 160, 100)}")
        first_choice = QLabel(fw_label)
        font = QFont()
        font.setBold(False)
        font.setPointSize(12)
        first_choice.setFont(font)
        first_choice.setText('优选')
        first_choice.setFrameStyle(QFrame.Shape.Box | QFrame.Shadow.Raised)
        first_choice.setLineWidth(2)
        first_choice.setAlignment(Qt.AlignmentFlag.AlignVCenter)
        first_choice.setWordWrap(True)
        first_choice.setGeometry(125, 4, 30, 45)

        first_choice.setStyleSheet(u"color: rgb(255, 0, 0);")
        for i in range(3):
            fw_image_label = QLabel(fw_label)
            fw_image_label.setGeometry(160 + i * 45, 10, 30, 30)
            fw_image_label.setObjectName(fw_t_list[i][:-4])
            fw_image_label.setStyleSheet("#%s{border-image: url(:/阵容等级图/%s)}" % (fw_t_list[i][:-4], fw_t_list[i]))
            fw_image_label.setToolTip(str(fw_t_list[i][:-4]) + ':' + str(tips_dict.get(fw_t_list[i][:-4])))
        first_second = QLabel(fw_label)
        font = QFont()
        font.setBold(False)
        font.setPointSize(12)
        first_second.setFont(font)
        first_second.setText('次选')
        first_second.setAlignment(Qt.AlignmentFlag.AlignVCenter)
        first_second.setWordWrap(True)
        first_second.setFrameStyle(QFrame.Shape.Box | QFrame.Shadow.Raised)
        first_second.setGeometry(285, 4, 30, 45)
        first_second.setLineWidth(2)
        first_second.setStyleSheet(u"color: rgb(255, 0, 0);")
        for i in range(3, len(fw_t_list)):
            fw_image_label = QLabel(fw_label)
            fw_image_label.setGeometry(190 + i * 45, 10, 30, 30)
            fw_image_label.setObjectName(fw_t_list[i][:-4])
            fw_image_label.setStyleSheet("#%s{border-image: url(:/阵容等级图/%s)}" % (fw_t_list[i][:-4], fw_t_list[i]))
            fw_image_label.setToolTip(str(fw_t_list[i][:-4]) + ':' + str(tips_dict.get(fw_t_list[i][:-4])))
        self.listbox.setCellWidget(1, 0, fw_label)
        #  以上完成对阵容详情页表格数据第二行内容的设置
        #  以下是对阵容详情页表格第三行数据设置
        self.listbox.setRowHeight(2, 100)  # 设置表格的第三行的高度
        eq_label = QLabel()
        eq_label.setFrameStyle(QFrame.Shape.Box | QFrame.Shadow.Raised)
        eq_label.setLineWidth(2)
        eq_label_left = QLabel(eq_label)
        font = QFont()
        font.setBold(True)
        font.setPointSize(12)
        eq_label_left.setFont(font)
        eq_label_left.setFrameStyle(QFrame.Shape.Box | QFrame.Shadow.Raised)
        eq_label_left.setLineWidth(2)
        eq_label_left.setGeometry(0, 0, 125, 30)
        eq_label_left.setObjectName('阵容装备')
        eq_label_left.setText('【阵容装备】')
        eq_label_left.setStyleSheet("#阵容装备{color: rgb(160, 160, 100)}")
        eq_label_mid_left = QLabel(eq_label)
        font = QFont()
        font.setBold(False)
        font.setPointSize(11)
        eq_label_mid_left.setFont(font)
        eq_label_mid_left.setGeometry(50, 30, 70, 25)
        eq_label_mid_left.setText('抢装顺序:')
        eq_label_mid_left.setObjectName('抢装顺序')
        eq_label_mid_left.setStyleSheet("#抢装顺序{color: rgb(255, 0, 0)}")
        eq_label_mid_image = QLabel(eq_label)
        eq_label_mid_image.setGeometry(130, 14, 210, 40)
        for i in range(len(eq_list)):
            eq_label_mid_right = QLabel(eq_label_mid_image)
            eq_label_mid_right.setGeometry(i * 45, 4, 30, 30)
            eq_label_mid_right.setObjectName(str(i))
            eq_label_mid_right.setToolTip(eq_list[i][:-4])
            eq_label_mid_right.setStyleSheet("#%s{border-image: url(:/装备图/%s)}" % (str(i), eq_list[i]))
            eq_label_mid_right1 = QLabel(eq_label_mid_image)
            eq_label_mid_right1.setText('>')
            eq_label_mid_right1.setGeometry(30 + i * 45, 5, 25, 25)
            font = QFont()
            font.setBold(False)
            font.setPointSize(16)
            eq_label_mid_right1.setFont(font)
            eq_label_mid_right1.setStyleSheet("color: rgb(255, 255, 0)")
        eq_label_lb = QLabel(eq_label)
        eq_label_lb.setGeometry(345, 10, 35, 40)
        font = QFont()
        font.setBold(False)
        font.setPointSize(11)
        eq_label_lb.setText('主 C装备')
        eq_label_lb.setFont(font)
        eq_label_lb.setWordWrap(True)
        eq_label_lb.setStyleSheet("color: rgb(255, 0, 0)")
        eq_label_b = QLabel(eq_label)
        eq_label_b.setGeometry(380, 7, 250, 40)
        eq_label_b_list = hero_eq_t_list[0]
        eq_label_b_hero = QLabel(eq_label_b)
        eq_label_b_hero.setGeometry(0, 4, 40, 40)
        eq_label_b_hero.setObjectName('头像')
        eq_label_b_hero.setToolTip(eq_label_b_list[0][:-4])
        eq_label_b_hero.setStyleSheet("#头像{border-image: url(:/英雄头像/%s)}" % eq_label_b_list[0])
        for i in range(1, 6):
            eq_label_b_eq = QLabel(eq_label_b)
            eq_label_b_eq.setGeometry(40 + 30 * (i - 1), 8, 25, 25)
            eq_label_b_eq.setObjectName(eq_label_b_list[i][:-4])
            eq_label_b_eq.setToolTip(eq_label_b_list[i][:-4])
            eq_label_b_eq.setStyleSheet(
                "#%s{border-image: url(:/装备图/%s)}" % (eq_label_b_list[i][:-4], eq_label_b_list[i]))
        # eq_label_b_info = QLabel(eq_label)
        # eq_label_b_info.setGeometry(590, 4, 100, 40)
        # eq_label_b_info.setText('前两件必备,后三件根据对局选其一')
        # eq_label_b_info.setWordWrap(True)
        # eq_label_b_info.setStyleSheet("color: rgb(0, 0, 255)")
        eq_label_b_info1 = QLabel(eq_label)
        eq_label_b_info1.setGeometry(18, 56, 100, 30)
        font = QFont()
        font.setBold(False)
        font.setPointSize(11)
        eq_label_b_info1.setText('其它英雄装备:')
        eq_label_b_info1.setFont(font)
        eq_label_b_info1.setWordWrap(True)
        eq_label_b_info1.setStyleSheet("color: rgb(255, 0, 0)")
        eq_label_other = QLabel(eq_label)
        eq_label_other.setGeometry(130, 55, 720, 43)
        n = 0
        s1 = 0
        s2 = 0
        for i in range(1, len(hero_eq_t_list)):
            for j in range(len(hero_eq_t_list[i])):
                if j == 0:
                    eq_label_hero1 = QLabel(eq_label_other)
                    eq_label_hero1.setGeometry(22 * n + s1 * 32 + s2 * 10, 10, 30, 30)
                    eq_label_hero1.setObjectName(hero_eq_t_list[i][0][:-4])
                    eq_label_hero1.setToolTip(hero_eq_t_list[i][0][:-4])
                    eq_label_hero1.setStyleSheet(
                        "#%s{border-image: url(:/英雄头像/%s)}" % (hero_eq_t_list[i][0][:-4], hero_eq_t_list[i][0]))
                    s1 += 1
                else:
                    eq_label_hero2 = QLabel(eq_label_other)
                    eq_label_hero2.setGeometry(22 * n + s1 * 32 + s2 * 10, 13, 22, 22)
                    eq_label_hero2.setObjectName(hero_eq_t_list[i][j][:-4])
                    eq_label_hero2.setToolTip(hero_eq_t_list[i][j][:-4])
                    eq_label_hero2.setStyleSheet(
                        "#%s{border-image: url(:/装备图/%s)}" % (hero_eq_t_list[i][j][:-4], hero_eq_t_list[i][j]))
                    n += 1
            eq_label_blank = QLabel(eq_label_other)
            eq_label_blank.setGeometry(30 * n + s1 * 45 + s2 * 15, 5, 10, 30)
            s2 += 1
        self.listbox.setCellWidget(2, 0, eq_label)
        #  以上完成阵容详情页表格第三行的完全设置
        #  以下是第四行的设置
        self.listbox.setRowHeight(3, 210)
        eq_label_4th = QLabel()
        # eq_label_4th.resize(399, 249)
        eq_label_4th_left = QLabel(eq_label_4th)
        eq_label_4th_left.setGeometry(0, 0, 300, 210)
        eq_label_4th_left.setFrameStyle(QFrame.Shape.Box | QFrame.Shadow.Raised)
        eq_label_4th_left.setLineWidth(2)
        eq_label_4th_left_label = QLabel(eq_label_4th_left)
        font = QFont()
        font.setBold(True)
        font.setPointSize(14)
        eq_label_4th_left_label.setFont(font)
        eq_label_4th_left_label.setGeometry(2, 2, 115, 35)
        eq_label_4th_left_label.setObjectName('中期站位')
        eq_label_4th_left_label.setText('【中期站位】')
        eq_label_4th_left_label.setStyleSheet("#中期站位{color: rgb(160, 160, 100)}")
        l_dict_m_n = dict_m_n.get(title[1:-1])[0]
        for m in range(4):
            i = 0 if m % 2 == 0 else 17
            for n in range(7):
                stay_label_left = QLabel(eq_label_4th)
                stay_label_left.setGeometry(5 + n * 40 + i, 40 + m * 40, 35, 35)
                stay_label_left.setFrameStyle(QFrame.Shape.Box | QFrame.Shadow.Raised)
                stay_label_left.setLineWidth(2)
                if (m, n) in l_dict_m_n.keys():
                    stay_label_left.setObjectName(l_dict_m_n.get((m, n)))
                    stay_label_left.setToolTip(l_dict_m_n.get((m, n)))
                    stay_label_left.setStyleSheet(
                        "#%s{border-image: url(:/英雄头像/%s.png)}" % (l_dict_m_n.get((m, n)), l_dict_m_n.get((m, n))))

        eq_label_4th_right = QLabel(eq_label_4th)
        eq_label_4th_right.setGeometry(302, 0, 300, 210)
        eq_label_4th_right.setFrameStyle(QFrame.Shape.Box | QFrame.Shadow.Raised)
        eq_label_4th_right.setLineWidth(2)
        eq_label_4th_right_label = QLabel(eq_label_4th_right)
        font = QFont()
        font.setBold(True)
        font.setPointSize(14)
        eq_label_4th_right_label.setFont(font)
        eq_label_4th_right_label.setGeometry(2, 2, 115, 35)
        eq_label_4th_right_label.setObjectName('后期站位')
        eq_label_4th_right_label.setText('【后期站位】')
        eq_label_4th_right_label.setStyleSheet("#后期站位{color: rgb(160, 160, 100)}")
        r_dict_m_n = dict_m_n.get(title[1:-1])[1]
        for m in range(4):
            i = 0 if m % 2 == 0 else 17
            for n in range(7):
                stay_label_right = QLabel(eq_label_4th)
                stay_label_right.setGeometry(305 + n * 40 + i, 40 + m * 40, 35, 35)
                stay_label_right.setFrameStyle(QFrame.Shape.Box | QFrame.Shadow.Raised)
                stay_label_right.setLineWidth(2)
                if (m, n) in r_dict_m_n.keys():
                    stay_label_right.setObjectName(r_dict_m_n.get((m, n)))
                    stay_label_right.setToolTip(r_dict_m_n.get((m, n)))
                    stay_label_right.setStyleSheet(
                        "#%s{border-image: url(:/英雄头像/%s.png)}" % (r_dict_m_n.get((m, n)), r_dict_m_n.get((m, n))))

        self.listbox.setCellWidget(3, 0, eq_label_4th)

        #  以上完成阵容详情页表格第四行的完全设置
        #  以下是第五行的设置
        self.listbox.setRowHeight(4, 70)
        eq_label_5th = QLabel()
        eq_label_5th_left = QLabel(eq_label_5th)
        font = QFont()
        font.setBold(True)
        font.setPointSize(14)
        eq_label_5th_left.setFont(font)
        eq_label_5th_left.setGeometry(0, 0, 115, 35)
        eq_label_5th_left.setObjectName('阵容分析')
        eq_label_5th_left.setText('【阵容分析】')
        eq_label_5th_left.setStyleSheet("#阵容分析{color: rgb(160, 160, 100)}")
        eq_label_5th_contain = QLabel(eq_label_5th)
        eq_label_5th_contain.setGeometry(125, 25, 500, 30)
        eq_label_5th_contain.setText('请自行分析,开发者没空,等有空分析了再添加.')
        font = QFont()
        font.setBold(True)
        font.setPointSize(12)
        eq_label_5th_contain.setFont(font)
        eq_label_5th_contain.setStyleSheet("color: rgb(0, 0, 255)")
        self.listbox.setCellWidget(4, 0, eq_label_5th)

    @staticmethod
    def win_max_min():
        global win_max_min_flag, MainWindow
        if win_max_min_flag:
            MainWindow.resize(660, 60)
            win_max_min_flag = False
        else:
            MainWindow.resize(660, 750)
            win_max_min_flag = True


class QMainWindow1(QMainWindow):

    def mousePressEvent(self, event):  ##事件开始
        if event.button() == QtCore.Qt.MouseButton.LeftButton:
            self.Move = True  ##设定bool为True
            self.Point = event.globalPosition().toPoint() - self.pos()  ##记录起始点坐标
            event.accept()

    def mouseMoveEvent(self, QMouseEvent: QMouseEvent):  ##移动时间
        if QtCore.Qt.MouseButton.LeftButton and self.Move:  ##切记这里的条件不能写死，只要判断move和鼠标执行即可！
            self.move(QMouseEvent.globalPosition().toPoint() - self.Point)  ##移动到鼠标到达的坐标点！
            QMouseEvent.accept()

    def mouseReleaseEvent(self, QMouseEvent):  ##结束事件
        self.Move = False


class DoubleClickLabel(QtWidgets.QLabel):

    def __init__(self, parent):
        super(DoubleClickLabel, self).__init__(parent=parent)
        self.f_case = self.parent().parent()

    def mouseDoubleClickEvent(self, event):
        global win_max_min_flag
        if win_max_min_flag:
            self.f_case.resize(660, 60)
            win_max_min_flag = False
        else:
            self.f_case.resize(660, 750)  # 使得label宽高恢复原样
            win_max_min_flag = True


if __name__ == '__main__':
    app = QApplication(sys.argv)
    MainWindow = QMainWindow1()  # 创建窗体对象
    ui = Ui_MainWindow()  # 创建PySide6设计的窗体对象
    ui.setupUi(MainWindow)  # 调用PySide6窗体的方法对窗体对象进行初始化设置
    win_max_min_flag = True
    MainWindow.show()
    sys.exit(app.exec())  # 程序关闭时退出进程
